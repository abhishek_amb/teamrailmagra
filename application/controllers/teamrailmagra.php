<?php

/**
 * Class Business
 * The note controller. Here we create, read, update and delete (CRUD) example data.
 */
class Teamrailmagra extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    public function __construct()
    {
        parent::__construct();

        // VERY IMPORTANT: All controllers/areas that should only be usable by logged-in users
        // need this line! Otherwise not-logged in users could do actions. If all of your pages should only
        // be usable by logged-in users: Put this line into libs/Controller->__construct
      //  Auth::handleLogin();
    }

    /**
     * This method controls what happens when you move to /note/index in your app.
     * Gets all Business info (of the user).
     */
    public function index()
    {
        $teamrailmagra_model = $this->loadModel('Teamrailmagra');
        $this->view->teamrailmagra = $teamrailmagra_model->getAllMembers();
        $this->view->render('teamrailmagra/index');
    }
	
	/*
	public function ChoiceAone()
    {
        $this->view->render('teamrailmagra/ChoiceA-one');
    }
	*/
	
	public function ourSponsered()
    {
        $this->view->render('teamrailmagra/ourSponsered');
    }
	
	public function RazaEmporiumRailmagra()
    {
        $this->view->render('teamrailmagra/RazaEmporiumRailmagra');
    }
	
	public function NeelKamalInfotechRailmagra()
	{
		$this->view->render('teamrailmagra/NeelKamalInfotechRailmagra');
	}
	
	
	
    /**
     * This method controls what happens when you move to /dashboard/create in your app.
     * Creates a new note. This is usually the target of form submit actions.
     */
    public function create()
    {
	
	
	
        // optimal MVC structure handles POST data in the controller, not in the model.
        // personally, I like POST-handling in the model much better (skinny controllers, fat models), so the login
        // stuff handles POST in the model. in this note-controller/model, the POST data is intentionally handled
        // in the controller, to show people how to do it "correctly". But I still think this is ugly.
		
		
		$target_dir = "public/img/teamrailmagra/";
		
	
$target_file = $target_dir . basename($_FILES["photo_url"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {

    $check = getimagesize($_FILES["photo_url"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["photo_url"]["size"] > 5000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["photo_url"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["photo_url"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
		
		
		
        
		if (isset($_POST['mobile_no']) AND !empty($_POST['mobile_no'])) {
            $teamrailmagra_model = $this->loadModel('Teamrailmagra');
            $teamrailmagra_model->create($_POST['name'],$_POST['occupation'],$_POST['mobile_no'],$_FILES["photo_url"]["name"]);
        }
       header('location: ' . URL . 'teamrailmagra');
    }

    /**
     * This method controls what happens when you move to /note/edit(/XX) in your app.
     * Shows the current content of the note and an editing form.
     * @param $note_id int id of the note
     */
    public function edit($note_id)
    {
        if (isset($note_id)) {
            // get the note that you want to edit (to show the current content)
            $note_model = $this->loadModel('Note');
            $this->view->note = $note_model->getNote($note_id);
            $this->view->render('note/edit');
        } else {
            header('location: ' . URL . 'note');
        }
    }

    /**
     * This method controls what happens when you move to /note/editsave(/XX) in your app.
     * Edits a note (performs the editing after form submit).
     * @param int $note_id id of the note
     */
    public function editSave($note_id)
    {
        if (isset($_POST['note_text']) && isset($note_id)) {
            // perform the update: pass note_id from URL and note_text from POST
            $note_model = $this->loadModel('Note');
            $note_model->editSave($note_id, $_POST['note_text']);
        }
        header('location: ' . URL . 'note');
    }

    /**
     * This method controls what happens when you move to /note/delete(/XX) in your app.
     * Deletes a note. In a real application a deletion via GET/URL is not recommended, but for demo purposes its
     * totally okay.
     * @param int $note_id id of the note
     */
    public function delete($note_id)
    {
        if (isset($note_id)) {
            $note_model = $this->loadModel('Note');
            $note_model->delete($note_id);
        }
        header('location: ' . URL . 'note');
    }
}
