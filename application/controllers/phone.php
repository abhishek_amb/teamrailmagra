<?php

/**
 * Class Business
 * The note controller. Here we create, read, update and delete (CRUD) example data.
 */
class Phone extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    public function __construct()
    {
        parent::__construct();

        // VERY IMPORTANT: All controllers/areas that should only be usable by logged-in users
        // need this line! Otherwise not-logged in users could do actions. If all of your pages should only
        // be usable by logged-in users: Put this line into libs/Controller->__construct
      //  Auth::handleLogin();
    }

    /**
     * This method controls what happens when you move to /note/index in your app.
     * Gets all Business info (of the user).
     */
    public function index()
    {
        $phone_model = $this->loadModel('Phone');
        $this->view->phone = $phone_model->getAllNumbers();
        $this->view->render('phone/index');
    }

    /**
     * This method controls what happens when you move to /dashboard/create in your app.
     * Creates a new note. This is usually the target of form submit actions.
     */
    public function create()
    {
	
	
	$other = '';
	if($_POST) {
		$other = $_POST['others'];
	}
        // optimal MVC structure handles POST data in the controller, not in the model.
        // personally, I like POST-handling in the model much better (skinny controllers, fat models), so the login
        // stuff handles POST in the model. in this note-controller/model, the POST data is intentionally handled
        // in the controller, to show people how to do it "correctly". But I still think this is ugly.
        if (isset($_POST['category_ID']) AND !empty($_POST['category_ID'])) {
            $phone_model = $this->loadModel('Phone');
            $phone_model->create($_POST['category_ID'],$_POST['name'],$_POST['mobile_no'],$_POST['phone_no'], $other);
        }
      header('location: ' . URL . 'phone');
    }

}
