<div class="container">
    <div class="col-lg-8 col-lg-offset-2 text-center">
        <div class="logo">
            <h1>OPPS, Error 404 !</h1>
        </div>

        <p class="text-muted">There is some error here, Please try later. </p>
        <div class="clearfix"></div>
        <br />
        <div class="col-lg-6  col-lg-offset-3">
            <div class="btn-group btn-group-justified">
                <a href="<?php echo URL; ?>" class="btn btn-success">Return Website</a>
              
            </div>

        </div>

    </div>


</div>