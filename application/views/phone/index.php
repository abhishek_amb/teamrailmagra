<script src="<?php echo URL;?>/public/js/popover.js"></script>

<div class="row">
<?php $this->renderFeedbackMessages(); ?>
    
	<div class="col-md-12">
      <h2 class="subHeadClass" >Important Phone Directory of <b>Railmagra</b> Upkhand:</h2>
	
      <p align="justify">This page contain the important contact no which provide guideline to users. If anyone wants to add their no. in directory. <br /> <b>#TeamRailmagra </b> </p>
      
    </div>
  </div>

 <section id="tables">
 
  <div class="page-header">
    <h3>List of Number</h3>
	<button class="btn btn-primary" data-toggle="modal" data-target="#login">
 Add Your Number
</button>
  </div>

  <table class="table table-bordered table-striped table-hover">
    <tbody>
	 <thead>
      <tr>
        <th>Sr</th>
        <th ></th>
		 <th>Mobile</th>
        <th >Phone</th>
		 <th >Category</th>
		 
      </tr>
    </thead>
	
    <tbody >
	<?php     
		if ($this->phone) {
				$j=1;
				$category_name = '';
                foreach($this->phone as $key => $value) {
				 if($value->is_premium == 1) {
				 echo '<tr style="background-color: #ecf0f1; color: black;"> <td>';
				echo $j ;
				 
				 echo '</td>';
		        
				echo ' <td>'; echo $value->name; echo '</td>';
				echo '<td>'; echo $value->mobile_no; 
				echo '<i class="fa fa-info-circle" id="jobskills'; echo $value->id; echo '" data-trigger="hover" data-container="body" data-toggle="popover"  data-placement="top" data-content="'; echo $value->descptn; echo '" data-html="true"></i>
                        <script>
                            $("#jobskills'; echo $value->id; echo'").popover();
                        </script>';
				
				
				
				
				
				
				
				
				
				
				echo
				
				
				
				
				'</td>';
				echo ' <td>'; echo $value->phone_no; echo'</td>';
				echo ' <td>'; echo $value->category_ID; echo'</td>';
		  
      echo '</tr>';						
	  }
	  else {
			 echo '<tr> <td>';
				echo $j ;
				 
				 echo '</td>';
		        
				echo ' <td>'; echo $value->name; echo '</td>';
				echo '<td>'; echo $value->mobile_no; echo'</td>';
				echo ' <td>'; echo $value->phone_no; echo'</td>';
				echo ' <td>'; echo $value->category_ID; echo'</td>';
		  
      echo '</tr>';	
	  }
                       
				 $j++;
				}
				}
				
		   ?>
	
	</tbody>
  </table>
  
<div class="page-header">
    <h6>List of Important Number</h6>
  </div>
</section>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">      
  <div class="modal-dialog">
	<div class="modal-content">
  	  <div class="modal-header text-center">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×    </button>
    	<h4 class="modal-title">Enter your Business information</h4>
  	  </div>
	  <div class="modal-body text-center">
        <form class="form-horizontal" role="form" method="post" action="<?php echo URL;?>phone/create">
			<div class="row">
            <label class="col-md-4">Category :</label>
			
            <div class="col-md-6">
             <select name="category_ID" class="form-control input-form" id="category_ID" onChange="check();">
					<option class="disabled" value="0">Choose Category</option>
					<option value="Advocate">Advocate</option>
					<option value="Bank">Bank</option>
					<option value="CA">CA</option>
					<option value="Clothing">Clothing</option>
					<option value="Computer(Sales OR Service)">Computer(Sales OR Service)</option>
					<option value="Hospital">Hospital</option>
					<option value="Ambulance">Ambulance</option>
					<option value="Medical Store">Medical Store</option>
					<option value="Others">Others</option>
					<option value="Post Office">Post Office</option>
					<option value="School">School</option>
			</select>
            </div>
          </div>
		 
		  <div class="row"  id="others" style="visibility:hidden" >
		  
            <label class="col-md-4">Others :</label>
            <div class="col-md-6">
              <input name="others" autocomplete="off" type="text" placeholder="please enter valid category" class="form-control" />
			<p style="color:green;">Sorry, we will add this in our category list. Thank You</p>
		   </div>
          </div>
		  
		  
		   <div class="row">
		    
            <label class="col-md-4">Name :</label>
            <div class="col-md-6">
              <input name="name" autocomplete="off" type="text" placeholder="Department or Your shop name" class="form-control" required />
            </div>
          </div>
		   <br>
          
		<div class="row">
            <label class="col-md-4">Mobile No:</label>
            <div class="col-md-6">
              <input name="mobile_no" autocomplete="off" type="text" pattern="[789][0-9]{9}" title="" placeholder="Enter valid mobile no in 10 digits" class="form-control" required />
			</div>
        </div>
		<br />
		  
		<div class="row">
            <label class="col-md-4">Phone No:</label>
            <div class="col-md-6">
              <input name="phone_no" autocomplete="off" type="text" placeholder="Enter phone No" class="form-control" required />
			</div>
        </div>
		  <br />
		  
          <input type="submit" class="btn btn-danger" value="Submit">
		  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </form>
      </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script>

function check() {
    var el = document.getElementById("category_ID");
    var str = el.options[el.selectedIndex].text;
    if(str == "Others") {
        show();
    }else {
        hide();
    }

}
function hide(){
    document.getElementById('others').style.visibility='hidden';
}
function show(){
    document.getElementById('others').style.visibility='visible';
}
</script>