<div class="container">
        <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Pranay Chulet (Quikr, Founder)</h2>

                   
                References <a href="http://en.wikipedia.org/wiki/Pranay_Chulet" title="Wikipedia, the free encyclopedia" target="_blank" >Wikipedia</a>
                    <br />
                    <br />
                    
                </div>
            </div>
         <!-- USER PROFILE ROW STARTS-->
            <div class="row">
                <div class="col-md-3 col-sm-3">
                                       
                    <div class="user-wrapper">
                        <img src="<?php echo URL;?>public/img/Pranay-quickr.jpg" class="img-responsive" /> 
                    <div class="description">
                       <h4> Mr <a href="http://en.wikipedia.org/wiki/Pranay_Chulet" title="Wikipedia, the free encyclopedia" target="_blank" > Pranay Chulet </a></h4>
                        <h5> <strong> Occupation : Businessperson </strong></h5>
                        <p>
                            Co-founder and the CEO of <a href="http://www.quikr.com/" target="_blank" >Quikr</a>
                        </p>
                        <hr />
                    </div>
                     </div>
                </div>
                
                <div class="col-md-9 col-sm-9  user-wrapper">
                    <div class="description">
                         <h3> Pranay's Biography : </h3>
                    <hr />
                     <p>
                          Pranay Chulet is an Indian entrepreneur and business executive. He is a co-founder and the CEO of Quikr, India's largest[citation needed] online and mobile classifieds portal.
                          <strong>Paranay Chulet attended the Kendriya Vidyalaya in Dariba, Rajasthan.</strong> He holds an undergraduate degree in chemical engineering from Indian Institute of Technology Delhi and an MBA from Indian Institute of Management Calcutta.. 
                         </p>
                    <p>
                         After graduating from IIM Calcutta, Chulet joined Procter & Gamble in a brand management position. He left P&G in 1997 to join Mitchell Madison Group as a management consultant. He later worked in a variety of roles in Walker Digital, PricewaterhouseCoopers and Booz Allen Hamilton.

Chulet started his first entrepreneurial venture, Excellere, in 2007. In 2008, he founded Kijiji India, which was later rebranded as Quikr. Quikr has raised over $150 million in funding from private equity and investment firms like Warburg Pincus, Omidyar Network, Matrix Partners, Norwest Venture Partners and Investment AB Kinnevik, and shareholders like eBay.
                        </p>   
                    <h3> Social Links: </h3>
                    <hr />                
                   <a href="https://www.facebook.com/pranay.chulet?fref=ts" class="btn btn-social btn-facebook">
                            <i class="fa fa-facebook"></i>&nbsp; Facebook </a>

                        <a href="https://twitter.com/pranayc" class="btn btn-social btn-twitter">
                            <i class="fa fa-twitter"></i>&nbsp; Twitter </a>
                        <a href="https://www.linkedin.com/pub/pranay-chulet/0/1a7/391" class="btn btn-social btn-linkedin">
                            <i class="fa fa-linkedin"></i>&nbsp; Linkedin </a>
                    </div>
                 
                </div>
            </div>
           <!-- USER PROFILE ROW END-->
    </div>