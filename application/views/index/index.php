	<!-- echo out the system feedback (error and success messages) -->
    <?php $this->renderFeedbackMessages(); ?>
<div id="carousel-example-generic" class="carousel slide" style="padding: 0px; margin: 0px;" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
             <img src="<?php echo URL; ?>/public/img/home/rajiv gandhi circle - railmagra - 313329 - www.railmagra.in.jpg" height="50" alt="First slide">
          </div>
          <div class="item">
            <img src="<?php echo URL; ?>/public/img/home/jaldevi mandir Sansera - railmagra.jpg" alt="Second slide">
          </div>
          <div class="item">
            <img src="<?php echo URL; ?>/public/img/home/railmagra - panchayat - office - 313329.jpg" alt="Third slide">
          </div>
		  <div class="item">
            <img src="<?php echo URL; ?>/public/img/home/surajbari mata rajpura - railmagra - 313329.jpg" alt="Fourth slide">
          </div>
        </div>
		
        <a class="left displayNone carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          &lsaquo;
        </a>
        <a class="right displayNone carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
         &rsaquo;
        </a>
      </div>
	<style>
	@media screen and (max-width: 768px) {
	 #img{
	 
	 height:100px;
	 
	 width:100px;
	 
	 }
	
	}
	
	
	</style>
	
     

 <div class="page-header">
  </div>

  <!-- Headings & Paragraph Copy -->
   <div class="container">
   <div class="main">
  <div class="row">
  
   <div class="col-md-9">
      <h3>Welcome to our website..</h3>
	  
	   <table class="table table-bordered table-striped table-hover">
    <tbody>
	 <thead>
      <tr>
        <th colspan="2" align="center">Basic Information</th>
      </tr>
    </thead>
	<tr>
		<td>Country</td>
		<td>India</td>
	</tr>
	<tr>
	<td>State::</td>
	<td><a target="_blank"  href="http://rajasthan.gov.in/">Rajasthan</a></td>
	</tr>
	<tr>
	<td>District(s)::</td>
	<td><a target="_blank"  href="http://rajsamand.rajasthan.gov.in">Rajsamand</a></td>
	</tr>
	<tr>
	<td>Nearest City::</td>
	<td> Udaipur['80 km'], Nathdwara['38 km']</td>
	</tr>
	<tr>
	<td>Assembly CONSTITUENCY::</td>
	<td>Nathdawara</td>
	</tr>
	<tr>
	<td> Lok Sabha constituency::</td>
	<td><a target="_blank"  href="http://rajsamand.rajasthan.gov.in">Rajsamand</a> </td>
	</tr>
	<tr>
	<td> Civic agency::</td>
	<td>sub-division of <a target="_blank"  href="http://rajsamand.rajasthan.gov.in">Rajsamand</a></td>
	</tr>
	<tr>
	<td>Time Zone::</td>
	<td>IST (UTC+5:30)</td>
	</tr>
	<tr>
	<td>Pincode::</td>
	<td>313329</td>
	</tr>
    </tbody >
	
	</table>
	
	</div>				



     
   
  
    <div class="col-md-2" id="only_shree">
          <img  rel="<?php echo URL; ?>teamrailmagra/NeelKamalInfotechRailmagra" src="<?php echo URL; ?>public/img/add/11.png" border="0">
		<!--  <img  rel="?php echo URL; ?>teamrailmagra/ChoiceAone" src="?php echo URL; ?>public/img/add/22.png" border="0">  -->
		  <img  rel="<?php echo URL; ?>teamrailmagra/RazaEmporiumRailmagra" src="<?php echo URL; ?>public/img/add/33.png" border="0">
		 <!-- <img  rel="#" src="?php echo URL; ?>public/img/add/44.png" border="0">  -->
		  
   </div>
   </div>
   
<div class="row">
 <div class="col-md-9">
 
 <p align="justify"> <a target="_blank"  href="http://www.railmagra.com">Railmagra</a> is a town and tehsil now sub-division (Upkhand) headquarters in Rajasmand district in the Indian state of Rajasthan. It is bounded on the north by Udaipur district, on the south by Bhilwara district, on the east by Chittorgarh district and has a good connectivity in all the ways.</p>
 <p align="justify">
 With a view to integrate all transport related facilities at one place to facilitate safe and efficient movement of road transport, the Ministry of Road Transport & Highways has proposed to set up a Transport Hub as a pilot project(under construction) at <a target="_blank"  href="http://www.railmagra.com">Railmagra</a> in District <a target="_blank"  href="http://rajsamand.rajasthan.gov.in">Rajsamand</a>, Rajasthan.
 </p>
 </div>
</div>
   
    <div class="row">
     <div class="col-md-9">
      <h3>Demographics:</h3>
	  <p align="justify">As of 2011 India census, <a target="_blank"  href="http://www.railmagra.com">Railmagra</a> had a population of 1,31,800 (66260 males and 65,540 females).Total number of households were 27874. 
Males constitute 52% of the population and females 48%. <a target="_blank"  href="http://www.railmagra.com">Railmagra</a> has an average literacy rate of 72% (2001 census),
 higher than the national average of 59.5%(2001 census): male literacy is 79%, and female literacy is 64%. In <a target="_blank"  href="http://www.railmagra.com">Railmagra</a>,
14% of the population is under 6 years of age.</p>
     
	 </div>
  </div>
  
    <div class="row">
     <div class="col-md-9">
      <h3>History:</h3>
	  <p align="justify">Sometimes with the name of town ‘<a target="_blank"  href="http://www.railmagra.com">Railmagra</a>’ it seems that it must have some ‘Rail’way tracks and ‘Magra’ (the regional name of hill)
after which it has been named. But the fact is it has neither a railway track nor a hill. There are three stories which are believed
to be the reason for its name.  </p>
 <p align="justify">The first story is that there was a stream (Rela) of Banas River which later met at the hills (Magra) of nearby village called Sindesar Kala.
By adjoining these two words say Rela and Magra it was better known as <a target="_blank"  href="http://www.railmagra.com">Railmagra</a>. The other story says that in the south-west of the town the vacant
land was known as ‘Magra’ and a persona named ‘Rela’ was used to protect it and later on with the combination of the two it named as <a target="_blank"  href="http://www.railmagra.com">Railmagra</a>. 
The final theory or story is there was the series of hills from Dariba to Sindesar Kala which seems like a rail of hills (Magra) and
probably with this view the town is called <a target="_blank"  href="http://www.railmagra.com">Railmagra</a>. </p>

     
	 </div>
  </div>
 
 
  
  <div class="row">
     <div class="col-md-9">
      <h3>Culture:</h3>
	  <p align="justify">
Main dialect of <a target="_blank"  href="http://www.railmagra.com">Railmagra</a> region is Mewari. It has many well developed villages like Dariba(HZL Zinc), Gilund, Kuraj.
it has  also included mewar region which is currently part of Udaipur district.   
 Major occupation in <a target="_blank"  href="http://www.railmagra.com">Railmagra</a> tehsil is agriculture where people  also prefer business due to the HZL zinc dariba.</p>
 <p align="justify">
The temple of Shree Devnarayan located at Pachmata[10 km],Jaldevi mata temple located at Sasera [13 km], Surajbari Mata temple(Rajpura),
Shree Charbhuja temple located at Sadri[7 km]
and the most famous Matrikundia('Mewar Ka Haridwar') temple is loacated at the border of <a target="_blank"  href="http://www.railmagra.com">Railmagra</a>.</p>

	 </div>
  </div>
  
    <div class="row">
     <div class="col-md-9">
      <h3>Education:</h3>
	  <p align="justify">
Many schools and colleges of both Hindi and English medium like JR College provide their services to community.
A Major schools are Modal School(Govt), Govt Senior Secondary school(at tehsil headquater,Science, arts and commerce), Vidya niketan Secondary School,
Bhartiya shishu niketan secondary school, Vivakand Secondary School, Garima Public School, <a target="_blank"  href="http://rajasthan.gov.in/">Rajasthan</a> public senior secondary school etc. Industrial
Training Institute for vocational training is located at <a target="_blank"  href="http://rajsamand.rajasthan.gov.in">Rajsamand</a> just 27 km away from tehsil headquarters.</p>

	 </div>
  </div>
  
  
      <div class="row">
     <div class="col-md-9">
      <h3>Geography:</h3>
	  <p align="justify">

It has a sub-division (Upkhand) which is located at 80 km from Udaipur district. Jaipur, the capital of <a target="_blank"  href="http://rajasthan.gov.in/">Rajasthan</a> is situated
at a distance of 320 km from Rajasmand district.</p>
  <p align="justify">
The area is characterized by semi arid with an average annual rainfall of about 650 mm, 
which is mainly received during monsoon season from July to September.
The minimum temperature in winter goes up to 2ºC and maximum 25ºC, while in summer, it goes up to minimum 20ºC and maximum 42ºC. 
Relative humidity in the area is above 70% during monsoon months but is below 20% during the months of March-May.
 The average rainfall of the area is about 624.5 mm.</p>
	 </div>
  </div>
  
        <div class="row">
     <div class="col-md-9">
      <h3>Industrial Area:</h3>
	  <p align="justify">
	  
	  The area is particularly rich in minerals resources like Zinc and asbestos found in the area. 
Green marble and soapstone mining are carried out through semi mechanized and manual methods by private company in this region.
The HZL plant is the Backbone of the area's employment opportunities and growth. The HZL has grown like anything in past few years and after the establishment of 
Smelting Plant the growth has been multipled. The Standard of Living and Lifestyle of locals have been improved and lots of infrastructue projects 
started in and around HZL. Vedanta Foundation (an NGO of Vedanta Group) has been working on various socio-economic development issues with the support from
local NGO's for the upliftment of underprivileged people in the area.  
</p>
	 </div>
  </div>
  
     <div class="row">
     <div class="col-md-9">
      <h3>Connectivity::</h3>
	  <p align="justify">
	  
	The distance from Delhi is 580 km and from Mumbai is 806 km. Town is well connected by the <a target="_blank"  href="http://rajasthan.gov.in/">Rajasthan</a>
state road transport services. Nearest railway station is Fatehnagar at a distance of 23 km.
Udaipur railway station is at distance of 80 km and trains are available for major routes. Nearest airport is Dabok at distance of 65 km.
</p>
	 </div>
  </div>
  
  

            <div class="col-xs-9 col-sm-9">

	
          
		            <div class="panel panel-primary">
		              <div class="panel-heading">
			               <h3 class="panel-title">Special Thanks to</h3>
			                </div>
            <div class="panel-body">
            <div class="row">
			 <div class="col-md-3" style="text-align: center;">
		 <img  id="img" style="height: 111px;" src="<?php echo URL; ?>public/img/website thank you_2.jpg" border="0">
		 </div>
		 
				  <div class="col-md-9">
				    <p align="justify">
         <b> रेलमगरा मार्गदर्शिका 2009</b>(सम्पादक संजय कुमार शर्मा C/o संजय कम्प्युटर्स, रेलमगरा)<br/>
          <b>Mr Roshan Lal Ji Tukliya</b>(Mahaveer Traders || Prem Publicity, +91-9414927728)<br/>
         <b> Mr Ramakant Ji Upadhyay</b>(Neelkamal Digital Photo Studio || +91-9413262651)<br/>
          <b>Mr CA Sunil Kumar Tak</b>(+91-7875041021)</p>
					  
				  </div>
			  
			  
          </div>
        </div>
		</div>
	
	</div>
	
	
	
	
	
	
  
	</div>

	</div>
 
</div>
 <br/>
 <br/>

 <script>
			$(document).ready(function() {
        var el = $('#only_shree'),
            top_offset = $('#only_shree').offset().top;

        $(window).scroll(function() {
          var scroll = $(this).scrollTop();

          if (scroll > top_offset) {
              el.css({"position":"fixed","top":"31px","right":"140px"});
          }
          else {
            el.css({"position":"static","top":"auto","right":"140px"});
          }
        });
			});
			
	
		</script>
 <script>

var only_shree_over = false;
var fadeIntervalshree;
var shree_delay = 3000;
function fadeonly_shree() {
   if(only_shree_over){return false;}
   var current_img = $("#only_shree img").eq(0);
   var next_img = current_img.next("img");        
   current_img.fadeOut().appendTo("#only_shree");        
   next_img.fadeIn();
        }
$("#only_shree img").each(function(){
    $(this).hide();
});
$("#only_shree img").eq(0).show();
$("#only_shree img").click(function(){
    window.location = $(this).attr("rel");
});

$("#only_shree img").hover(function(){
    only_shree_over = true;
    clearInterval(fadeIntervalshree)
    },function(){
    only_shree_over = false;
    fadeIntervalshree = setInterval(fadeonly_shree, shree_delay);    
    })
    
  fadeIntervalshree = setInterval(fadeonly_shree, shree_delay);     
</script>
