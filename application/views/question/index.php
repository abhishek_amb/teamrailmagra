<script src="<?php echo URL;?>/public/js/bootstrap-tooltip.js"></script>
<script src="<?php echo URL;?>/public/js/bootstrap-popover.js"></script>

<div class="row">
<?php $this->renderFeedbackMessages(); ?>

	<div class="col-md-12">
      <h3>Questions and answer page:</h3>
	
      <p align="justify">This "Questions and answer" page has provide facility for those students(specially 10th OR 12th), who are confused about their career. They can ask their query or question regarding their career. Our expert team will resolved their queries within 24 to 48 hours.<br/> <b> Thank you #TeamRailmagra </b>    </p>
       
    </div>
  </div>
<button class="btn btn-success" data-toggle="modal" data-target="#question_form">
 Add your question
</button>


   <div class="row">
  <div class="col-md-12">
  <br />
          <div class="list-group">
            
			<?php     
			
		if ($this->question) {
				$j=1;
                foreach($this->question['question'] as $key => $value) { 
				
				?>
             <span class="list-group-item">
              <div class="row"> 
			  <div class="col-md-12">
			  <img id="kk7"  height="50px" width="50px" class="img-thumbnail" src="<?php echo URL; ?>/public/img/people.png" alt="Apple">
			  <span>&nbsp;<?php echo $value->posted_by ?>&nbsp;says</span>
			  </div>
			  </div>
			  <br/>
			   <div class="row"> 
			   <div class="col-md-12">
			  <div class="well">
			  <h4 class="list-group-item-heading"><?php echo $value->question ?></h4>
			  </div>
			  </div>
			  </div>
			  <?php 
 foreach($this->question['answer'] as $key1 => $value1) {
	if($value->id == $value1->question_id) {
				?>
              <p class="list-group-item-text">
			  <br/>
			  <div class="row">
	<div class="col-md-1"><img height="40px" width="40px" src="<?php echo URL; ?>/public/img/reply.jpg" alt="Reply"></div>
	<div class="col-md-11"><?php echo $value1->answer; ?></div></div>
	<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-2"><i class="fa fa-thumbs-o-up"></i><?php echo $value1->like_count; ?>&nbsp;&nbsp;<i class="fa fa-thumbs-o-down"></i> <?php echo $value1->unlike_count; ?> </div>
	<div class="col-md-3">
	<span>Posted by:<b>&nbsp;<?php echo $value1->posted_by; ?> </b> </span>
	
	</div>
	<div class="col-md-6">
	
	<span>Posted date:<b>&nbsp;<?php echo $value1->posted_date; ?></b></span>
	</div>
	</div>
	<br/>
	<div class="row" style="border-bottom:1px solid lightgrey">
	</div>
	
<?php

}
$j++;
}
echo '
 <br />
<button  style="text-algin:right" class="open-AddBookDialog btn btn-primary" data-toggle="modal" data-id="'; echo $value->id ; echo '" data-target="#reply_form">
 Reply
</button>';?>

			  </p>
           </span>
<?php
}
}
 ?>  			  
			  
         
          </div>
        </div>
		</div>
	
<div class="modal fade" id="question_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">      
  <div class="modal-dialog">
	<div class="modal-content">
  	  <div class="modal-header text-center">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×    </button>
    	<h4 class="modal-title">Ask your question here</h4>
  	  </div>
	  <div class="modal-body text-center">
        <form class="form-horizontal" role="form" method="post" action="<?php echo URL;?>question/create">
		<div class="form-group">
			 <div class="row">
				<label class="col-md-3">Your Name:</label>
					<div class="col-md-8">
						<input name="name" autocomplete="off" type="text" placeholder="Enter your name here.." class="form-control" required />
					</div>
			 </div>
		 </div>
			  <br />
		<div class="form-group">
			 <div class="row">
				<label class="col-md-3">Your Email:</label>
					<div class="col-md-8">
						<input name="email" autocomplete="off" type="text" placeholder="Enter your email here.." class="form-control" required />
					</div>
			 </div>
		   <br />
		</div>
		<div class="form-group">
			<div class="row">
				<label class="col-md-3">Question:</label>
					<div class="col-md-8">
						<textarea rows="4" cols="40" autocomplete="off" maxlength="2000" placeholder="Enter your question here.." name="question" required class="form-control"></textarea>
					</div>
			</div>
		</div>
		  
		  <div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-2">
				<input type="submit" class="btn btn-danger" value="Submit">
			</div>
			<div class="col-md-2">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		  </div>
        </form>
      </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="reply_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">      
  <div class="modal-dialog">
	<div class="modal-content">
  	  <div class="modal-header text-center">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×    </button>
    	<h4 class="modal-title">Reply to this question</h4>
  	  </div>
	  <div class="modal-body text-center">
        <form class="form-horizontal" role="form" method="post" action="<?php echo URL;?>question/answer">
		<div class="form-group">
			 <div class="row">
				<label class="col-md-3">Your Name:</label>
					<div class="col-md-8">
						<input name="name" autocomplete="off" type="text" placeholder="Enter your name here.." class="form-control" required />
						<input name="id" id="question_id" style="display:none;" type="text" placeholder="Enter your name here.." class="form-control" required />
					</div>
			 </div>
		 </div>
			  <br />
		<div class="form-group">
			 <div class="row">
				<label class="col-md-3">Your Email:</label>
					<div class="col-md-8">
						<input name="email" autocomplete="off" type="text" placeholder="Enter your email here.." class="form-control" required />
					</div>
			 </div>
		   <br />
		</div>
		<div class="form-group">
			<div class="row">
				<label class="col-md-3">Answer:</label>
					<div class="col-md-8">
						<textarea rows="4" cols="40" autocomplete="off" maxlength="2000" placeholder="Enter your answer here.." name="answer" required class="form-control"></textarea>
					</div>
			</div>
		</div>
		  
		  <div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-2">
				<input type="submit" class="btn btn-danger" value="Submit">
			</div>
			<div class="col-md-2">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		  </div>
        </form>
      </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
$(document).on("click", ".open-AddBookDialog", function () {
     var myBookId = $(this).data('id');
     $("#question_id").val( myBookId );
     // As pointed out in comments, 
     // it is superfluous to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});
</script>