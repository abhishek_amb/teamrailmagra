<div class="container">
      <!-- Example row of columns -->
      <div class="row">
			<div class="col-sm-12 col-lg-12">
				<div class="panel">
					<div class="panel-heading">
						<h3><i class="fa fa-map-marker"></i> Location</h3>
					</div>
					<div class="panel-body">
					  <!-- gMap script container !Do not remove!! -->
							  
							  <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14461.308843591836!2d74.11509985!3d25.022967350000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3968649be09c2cdd%3A0xbe7c84c3e9a37bc4!2sRelmangra%2C+Rajasthan+313329!5e0!3m2!1sen!2sin!4v1418497840783"  frameborder="0" style="border:0"></iframe>
							  	 
					  <!-- gMap script container !Do not remove!! -->
					</div>
				  </div>
			</div>
		</div>
	<hr>
	<div class="row">
			<div class="col-sm-4 col-lg-4">
				<div class="panel">
					<div class="panel-heading">
						<h3><i class="fa fa-suitcase"></i> Our office</h3>
					</div>
					<div class="panel-body">
						<address>
						<strong>#TeamRailmagra</strong><br>
						<strong>Shree Goverdhan Web Solutions</strong><br>
						Hospital Road, Railmagra 313329<br>
						<br>
						<i class="fa fa-phone-square"></i> + 91 9660-245223 <br />
						<i class="fa fa-phone-square"></i> + 91 8852-071802 <br />
						<i class="fa fa-envelope"></i>  info@railmagra.com
						</address>
					</div>
				</div>
			
			<div class="panel">
				<div class="panel-heading">
				  <h3><i class="fa fa-clock-o"></i> Business hours</h3>
				</div>
				<div class="panel-body">
					<table class="table table-hover">
					  <thead>
						<tr>
						  
						  <th>Day</th>
						  <th>Time</th>
						</tr>
					  </thead>
					  <tbody>
						<tr class="success">
						  <td>Monday to Saturday</td>
						  <td>7:00 Am to 9:00 Am(Morning) || 7.00 Pm to 9.00 Pm(Evening)</td>
						</tr>
						
						<tr class="danger">
						
						  <td>Sunday</td>
						  <td>Any time</td>
						</tr>
					  </tbody>
					</table>
					</div>
				</div>
			</div>
       <div class="col-12 col-lg-8">
	  
			<div class="panel">
					<div class="panel-heading">	
			<h3 class="">
				<i class="fa fa-envelope"></i>
				Feel free to contact us
			</h3>
			</div>
			<div class="panel-body">
			<!-- CONTACT FORM https://github.com/jonmbake/bootstrap3-contact-form -->
			
			<form role="form" method="post" action="<?php echo URL;?>contact/create" id="feedbackForm">
			      <div class="form-group">
				<input type="text" class="form-control" id="name" name="name" placeholder="Name" required />
				<span class="help-block" style="display: none;">Please enter your name.</span>
			      </div>
			      <div class="form-group">
				<input type="email" class="form-control" id="email" name="Email" placeholder="Email Address" required />
				<span class="help-block" style="display: none;">Please enter a valid e-mail address.</span>
			      </div>
				   <div class="form-group">
				<input type="text" class="form-control" id="mobile" name="mobile_no" placeholder="Mobile No(Optional)">
			      </div>
			      <div class="form-group">
				<textarea rows="10" cols="100" class="form-control" id="message" name="Message" placeholder="Message" required /></textarea>
				<span class="help-block" style="display: none;">Please enter a message.</span>
			      </div>
				  
			      <button type="submit" id="feedbackSubmit" class="btn btn-primary btn-lg" style="display: block; margin-top: 10px;">Send Feedback</button>
			    </form>
			<!-- END CONTACT FORM -->
			</div>
			</div>			
		</div>
      </div>
	  </div>


