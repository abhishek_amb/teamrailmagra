<div id="counter" class="powr-hit-counter" label="Thank You For Visiting Us"></div>
 </div> <!-- /container -->
 
 <footer>
		
      <div class="container">
        <p>© copyright 2014  <a href="http://www.railmagra.com/" rel="author"> www.railmagra.com </a> powered by  Shree Goverdhan Web Solutions</p>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
	
	<!-- Add fancyBox main JS and CSS files -->
	<!--<script type="text/javascript" src="<php echo URL; ?>public/js/jquery.fancybox.js?v=2.1.5"></script>-->
<script type="text/javascript">
/*
(function() {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = '<?php echo URL; ?>public/js/jquery.fancybox.min.js?v=2.1.5';
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
})();
*/
</script>

<script type="text/javascript">
(function() {
    var boot = document.createElement('script');
    boot.type = 'text/javascript';
    boot.async = true;
    boot.src = '<?php echo URL; ?>public/js/bootstrap.min.js';
    var bootJs = document.getElementsByTagName('script')[0];
    bootJs.parentNode.insertBefore(boot, bootJs);
})();
</script>


<script type="text/javascript">
(function() {
    var bootSmooth = document.createElement('script');
    bootSmooth.type = 'text/javascript';
    bootSmooth.async = true;
    bootSmooth.src = '<?php echo URL; ?>public/js/jquery.smooth-scroll.min.js';
    var bootSmoothJs = document.getElementsByTagName('script')[0];
    bootSmoothJs.parentNode.insertBefore(bootSmooth, bootSmoothJs);
})();
</script>
    <!-- Placed at the end of the document so the pages load faster -->
	
<!-- <script src="<php echo URL; ?>public/js/bootstrap.min.js"></script> -->
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug 
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
<script src="<?php echo URL; ?>public/js/jquery-blink.min.js" language="javscript" type="text/javascript"></script>
<!--<script src="?php echo URL; ?>public/js/jquery.smooth-scroll.min.js"></script>-->
<script type="text/javascript">


$(document).ready(function() {
    //this is the useful function to scroll a text inside an element...
    function startScrolling(scroller_obj, velocity, start_from) {
        //bind animation  inside the scroller element
        scroller_obj.bind("marquee", function (event, c) {
            //text to scroll
            var ob = $(this);
            //scroller width
            var sw = parseInt(ob.parent().width());            

            //text width
            var tw = parseInt(ob.width());

            tw = tw - 10;
            //text left position relative to the offset parent
            var tl = parseInt(ob.position().left);
            //velocity converted to calculate duration
            var v = velocity > 0 && velocity < 100 ? (100 - velocity) * 1000 : 5000;
            //same velocity for different text"s length in relation with duration
            var dr = (v * tw / sw) + v;
                        
            //is it scrolling from right or left?
            switch (start_from) {
                case "right":
                    //is it the first time?
                    if (typeof c == "undefined") {
                        //if yes, start from the absolute right
                        ob.css({
                            left: (sw - 10)
                        });
                        sw = -tw;
                    } else {
                        //else calculate destination position
                        sw = tl - (tw + sw);
                    };
                    break;
                default:
                    if (typeof c == "undefined") {
                        //start from the absolute left
                        ob.css({
                            left: -tw
                        });
                    } else {
                        //else calculate destination position
                        sw += tl + tw;
                    };
            }
            //attach animation to scroller element and start it by a trigger
            ob.animate({
                left: sw
            }, {
                duration: dr,
                easing: "linear",
                complete: function () {                    
                    ob.trigger("marquee");
                },
                step: function () {
                    // check if scroller limits are reached
                    if (start_from == "right") {
                        if (parseInt(ob.position().left) < -parseInt(ob.width())) {
                            //we need to stop and restart animation
                            ob.stop();
                            ob.trigger("marquee");
                        };
                    } else {
                        if (parseInt(ob.position().left) > parseInt(ob.parent().width())) {
                            ob.stop();
                            ob.trigger("marquee");
                        };
                    };
                }
            });
        }).trigger("marquee");
        //pause scrolling animation on mouse over
        scroller_obj.mouseover(function () {
            $(this).stop();
        });
        //resume scrolling animation on mouse out
        scroller_obj.mouseout(function () {
            $(this).trigger("marquee", ["resume"]);
        });
    };

    //the main app starts here...

    //settings to pass to function
    var scroller = $(".scrollingtext"); // element(s) to scroll
    var scrolling_velocity = 80; // 1-99
    var scrolling_from = "right"; // "right" or "left"

    //call the function and start to scroll..
    startScrolling(scroller, scrolling_velocity, scrolling_from);
});
</script>
<script type="text/javascript" language="javascript">
/*
$(document).ready(function()
{
			function JQuryBox(){
				var date = new Date;
				var minutes = date.getMinutes();
				var localMinute = localStorage.getItem('localMinute');
				if(!localMinute) {
				$.fancybox.open({
					href : '<?php echo URL; ?>public/js/iframe.html',
					type : 'iframe',
					'width': 660,
					'height': 1000,
					padding : 5,
					  'titleShow': false,               
               'autoscale': false,               
               'autoDimensions': false  
				});
				localStorage.setItem('localMinute', minutes);
				}
				
				if((minutes - localMinute) >= 4) {
				$.fancybox.open({
					href : '<?php echo URL; ?>public/js/iframe.html',
					type : 'iframe',
					'width': 660,
					'height': 1000,
					padding : 5,
					  'titleShow': false,               
               'autoscale': false,               
               'autoDimensions': false  
				});
				localStorage.setItem('localMinute', minutes);
				}
			}
			window.onload = JQuryBox;
			$(".blink").blink();

});
*/
</script>
<script>
var over = false;
var fadeInterval;
var delay = 3000;
function fadeMovies() {
   if(over){return false;}
   var current_img = $("#movies img").eq(0);
   var next_img = current_img.next("img");        
   current_img.fadeOut().appendTo("#movies");        
   next_img.fadeIn();
}
$("#movies img").each(function(){
    $(this).hide();
});
$("#movies img").eq(0).show();
$("#movies img").click(function(){
    window.location = $(this).attr("rel");
});

$("#movies img").hover(function(){
    over = true;
    clearInterval(fadeInterval)
    },function(){
    over = false;
    fadeInterval = setInterval(fadeMovies, delay);    
    })    
fadeInterval = setInterval(fadeMovies, delay);    



</script>
<!--
<script src="//www.powr.io/powr.js" powr-token="9ac591f067" external-type="html"></script>
-->

<script type="text/javascript">
(function() {
    var hitCounter = document.createElement('script');
	hitCounter.setAttribute('powr-token','9ac591f067');
	hitCounter.setAttribute('src','//www.powr.io/powr.js');
	hitCounter.setAttribute('external-type','html');
	 hitCounter.async = true;
    //hitCounter.src = '//www.powr.io/powr.js';
    var hitCounterJs = document.getElementsByTagName('script')[0];
    hitCounterJs.parentNode.insertBefore(hitCounter, hitCounterJs);
})();
</script>
  </body>
</html>