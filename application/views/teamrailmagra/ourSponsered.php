<script src="<?php echo URL;?>/public/js/bootstrap-tooltip.js"></script>
<script src="<?php echo URL;?>/public/js/bootstrap-popover.js"></script>


<div class="row">
<?php $this->renderFeedbackMessages(); ?>
    
	<div class="col-md-12">
      <h3><b>Our Sponsored:</b></h3>
	
      <h2 style="text-align:center;color:green;"><b>Thanks to all our sponsored to support #TeamRailmagra </b> </h2></p>
      
	  
    </div>
  </div>

 <section id="tables">
 
  <div class="page-header">
   <h4 style="text-align:center;color:blue;"><b><u>If you are interested to become sponsored for support #TeamRailmagra then hit "Join Us" Button. Thank you </u></b></h3> <br/>
	<button class="btn btn-primary" data-toggle="modal" data-target="#login">
 Join Us
</button>
  </div>

  <table class="table table-bordered table-striped table-hover">
    <tbody>
	 <thead>
      <tr>
        <th>Sr</th>
        <th >Shop Name</th>
		 <th>Contact</th>
        <th >Current Plan</th>
		
      </tr>
    </thead>
	
    <tbody >
	<tr>
		<td>1</td>
		<td><b>Neelkamal Infotech, Railmagra</b></td>
		<td>M.no +91-9460113389</td>
		<td>Platinum <i style="color:red;" class="fa fa-star"></i></td>
	</tr>
	<!--
	<tr>
		<td>2</td>
		<td><b>Choice A-1 Welding Fabrication Railmagra</b></td>
		<td>M.no +91-9828744383</td>
		<td>Platinum <i style="color:red;" class="fa fa-star"></i></td>
	</tr>
	-->
	
	<tr>
		<td>2</td>
		<td><b>Raza Emporium, Railmagra</b></td>
		<td>M.no +91-9414685688</td>
		<td>Platinum <i style="color:red;" class="fa fa-star"></i></td>
	</tr>
	
	<tr>
		<td>3</td>
		<td><b>TUKLIYA & CO, CHARTERED ACCOUNTANT, Railmagra</b></td>
		<td>M.no +91-9414927728</td>
		<td>Gold <i style="color:red;" class="fa fa-star-half-o"></i></td>
	</tr>
	</tbody>
  </table>
  
<div class="page-header">
    <h6>#TeamRailmagra</h6>
  </div>
</section>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">      
  <div class="modal-dialog">
	<div class="modal-content">
  	  <div class="modal-header text-center">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×    </button>
    	<h4 class="modal-title">Enter your Business information</h4>
  	  </div>
	  <div class="modal-body text-center">
        <form class="form-horizontal" role="form" method="post" action="<?php echo URL;?>business/create">
		<div class="form-group">
			<div class="row">
            <label class="col-md-4">Category :</label>
			
            <div class="col-md-6">
             <select name="category_ID" class="form-control input-form" id="category_ID" required>
					<option class="disabled" value="0">Choose Category</option>
					<option value="1">Computer(Sales OR Service)</option>
					<option value="10">Electronics</option>
					<option value="4">Clothing</option>
					<option value="11">Mobile(Sales OR Service)</option>
					<option value="3">Motorcycle(Sales OR Service)</option>
					<option value="7">RealEtate</option>
					<option value="8">Sweet Corner</option>
					<option value="Others">Others</option>
			</select>
            </div>
          </div>
		  <br>
		  <div class="row">
            <label class="col-md-4">Shop Name :</label>
            <div class="col-md-6">
              <input name="shop_name" autocomplete="off" type="text" placeholder="ShopName" title="Enter valid Shop Name" class="form-control" required />
            </div>
          </div>
		   <br>
          
			  <div class="row">
            <label class="col-md-4">Mobile No:</label>
            <div class="col-md-6">
              <input name="mobile_no" autocomplete="off" type="text" pattern="[789][0-9]{9}" title="Enter valid mobile no in 10 digits" placeholder="Mobile No" class="form-control" required />
            </div>
			
          </div>
		  <br>
            <label class="col-md-4">Address:</label>
            <div class="col-md-6">
			  <textarea rows="4" cols="40" autocomplete="off" maxlength="2000" name="address" required class="form-control"></textarea>
            </div>
          </div>
		  
		  <br>
          <div class="form-group">
		   <div class="row">
            <label class="col-md-4">Plan:</label>
			
            <div class="col-md-6">	
					<label class="btn btn-primary">
						<input type="radio"  name="is_premium" value="1" id="is_premium1" > Premium
					</label>
            </div>
			  </div>
			
          </div>
         <br>
		 <div class="form-group" id="competeyes" >
			<div class="row">
            <label class="col-md-4">Description:</label>
            <div class="col-sm-6">
             <textarea rows="4" placeholder="Enter your business description(Not Compulsory)." cols="40" maxlength="2000" name="descptn" class="form-control"></textarea>
			 <p style="color:red">we will call you within 24hours for more details.</p>
            </div>
			 </div>
          </div>
         <br>
		 
		 
          
          <input type="submit" class="btn btn-danger" value="Submit">
		  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </form>
      </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

