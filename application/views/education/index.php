<script src="<?php echo URL;?>/public/js/popover.js"></script>


<div class="row">
<?php $this->renderFeedbackMessages(); ?>
    
	<div class="col-md-12">
      <h3>Education Page:</h3>
	
      <p align="justify">There was only a Govt. Higher Secondary School and Govt. Girls Higher Secondary School in the town which has also given the educationist like Mr. Mohan Lal Mehta which after doing his higher education worked as Public Accountant in USA. Presently, the town is a education hub for the whole sub division with its wide and versatile range of schools for different classes of people. It has a number of Primary, Secondary and Higher Secondary Schools along with the J.R. College of Arts.  </p>
       <p align="justify">For the bright future of your children the list of various govt. And private schools are as follows: </p>
	 
    </div>
  </div>
  
<section id="tables">
 
  <div class="page-header">
    <h3>List of School's / College /Coaching Institutes</h3>
	<button class="btn btn-primary" data-toggle="modal" data-target="#login">
 Add New Education Sector
</button>
  </div>

  <table class="table table-bordered table-striped table-hover">
    <tbody>
	 <thead>
      <tr>
        <th>Sr</th>
        <th > Name</th>
		<th > Type</th>
		 <th>Contact No</th>
        <th >Address</th>
		 <th></th>
      </tr>
    </thead>
    <tbody >
	<?php     
		if ($this->education) {
				$j=1;
				$isSchool = '';
				$type = '';
                foreach($this->education as $key => $value) {
				 echo '<tr> <td>';
				echo $j ;
				if($value->category_edu ==0) {
					$isSchool = '<i class="fa fa-university"></i>';
					$type = 'School';
				}
				if($value->category_edu ==1) {
					$isSchool = '<i class="fa fa-graduation-cap"></i>';
					$type = 'College';
				}
				if($value->category_edu ==2) {
					$isSchool = '<i class="fa fa-university"></i>';
					$type = 'Coaching Centre';
				}
				 echo '</td>';
		        
                        if($value->is_premium ==1) {
                        
						 
						echo '<td >'; echo $value->school_name; echo $isSchool; echo '<i class="fa fa-info-circle" id="jobskills'; echo $value->ID; echo '" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="top" data-content="'; echo $value->descptn; echo '" data-html="true"></li></td>
						<script>
						$("#jobskills'; echo $value->ID; echo'").popover();
						</script>';
						 } else { 
          echo '<td>'; echo $value->school_name; echo '</td>';
		                }
echo ' <td>'; echo $type; echo '('.$isSchool.')'; echo'</td>';
echo ' <td>'; echo $value->mobile_no; echo'</td>
		  <td>'; echo $value->address; echo'</td>';
		  
		  
		 if($value->link_url) { 
		  echo '<td> <a class="btn btn-primary" href="'; $value->link_url; echo '">More..</a></td>';
		   } else { 
		  echo  '<td> </td>';
			 } 
      echo '</tr>';						
                       
				 $j++;
				}
	       }
		?>
	
	
</tbody>
  </table>
  
<div class="page-header">
    <h6>List of School</h6>
  </div>
</section>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">      
  <div class="modal-dialog">
	<div class="modal-content">
  	  <div class="modal-header text-center">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×    </button>
    	<h4 class="modal-title">Enter School information</h4>
  	  </div>
	  <div class="modal-body text-center">
        <form class="form-horizontal" role="form" method="post" action="<?php echo URL;?>education/create">
		<div class="row">
            <label class="col-md-4">Education Category :</label>
            <div class="col-md-6">
             <select name="category_edu"  class="form-control input-form" id="category_edu" onchange='checkCategory(this.value);'>
					<option class="disabled" value="0">Choose Category</option>
					<option value="0">School</option>
					<option value="1" disabled>College</option>
					<option value="2">Coaching Centre</option>
				
			</select>
            </div>
          </div>
		  <br>
		<div class="row">
            <label class="col-md-4">Managed by :</label>
            <div class="col-md-6">
             <select name="is_gov"  class="form-control input-form" id="is_gov" required>
					<option class="disabled" value="0">Choose Category</option>
					<option value="1">Government</option>
					<option value="2">Private</option>
				
			</select>
            </div>
          </div>
		  <br>
          <div class="row" id="school_div">
            <label class="col-md-4">School Name :</label>
            <div class="col-md-6">
              <input name="school_name" autocomplete="off" type="text" placeholder="SchoolName" title="Enter valid School Name" class="form-control"  />
            </div>
          </div>
		   <div class="row" id="coaching_div">
            <label class="col-md-4">Coaching Centre Name :</label>
            <div class="col-md-6">
              <input name="school_name" autocomplete="off" type="text" placeholder="Coaching Centre Name" title="Enter valid Coaching Name" class="form-control"  />
            </div>
          </div>
		   <br>
          <div class="row">
            <label class="col-md-4">Contact No:</label>
            <div class="col-md-6">
              <input name="mobile_no" autocomplete="off" type="text" title="Enter your contact no" placeholder="Contact No" class="form-control" required />
            </div>
          </div>
		  <br>
          <div class="row">
            <label class="col-md-4">Address:</label>
            <div class="col-md-6">
			  <textarea rows="4" cols="40" autocomplete="off" maxlength="2000" name="address" required class="form-control"></textarea>
            </div>
          </div>
		  <br>
          <div class="row">
            <label class="col-md-4">Plan:</label>
            <div class="col-md-6">	
					<label class="btn btn-primary">
						<input type="radio"  name="is_premium" value="0" id="is_premium" checked> Free
					</label>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<label class="btn btn-primary">
						<input type="radio"  name="is_premium" value="1" id="is_premium1" > Premium
					</label>
            </div>
			
          </div>
         <br>
		 <div class="row" id="competeyes" style="display:none">
            <label class="col-md-4">Description:</label>
            <div class="col-md-6">
             <textarea rows="4" cols="40" maxlength="2000" name="descptn" class="form-control"></textarea>
			 <p style="color:red">we will call you within 24hours for more details.</p>
            </div>
          </div>
         <br>
		 
		 
          
          <input type="submit" class="btn btn-danger" value="Submit">
		  
		  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </form>
      </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script>

document.getElementById("is_premium1").addEventListener("click", function() {
       document.getElementById("competeyes").style.display = "block";
    });

	document.getElementById("is_premium").addEventListener("click", function() {
       document.getElementById("competeyes").style.display = "none";
    });
	$("#coaching_div").hide();
	function checkCategory(val){
 var element=document.getElementById('is_gov');
 if(val== 2) {
    $("#is_gov").prop('disabled', true);
	$("#coaching_div").show();
	$("#school_div").hide();
	}
 else { 
   $("#is_gov").prop('disabled', false);
   $("#coaching_div").hide();
   $("#school_div").show();
}
}
 </script>
