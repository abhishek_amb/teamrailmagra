<script src="<?php echo URL;?>/public/js/bootstrap-tooltip.js"></script>
<script src="<?php echo URL;?>/public/js/bootstrap-popover.js"></script>


<div class="row">
<?php $this->renderFeedbackMessages(); ?>
    
	<div class="col-md-12">
      <h3>सरपंच - 2015 की आरक्षित सूची ( पंचायत समिति, रेलमगरा )  :</h3>

      <p align="justify">श्रीमान रमेश चन्द्र जी वीरवाल साहब का कोटि-कोटि धन्यवाद, महत्वपूर्ण जानकारी उपलब्ध करवाने के लिए</p>
       <p align="justify"><font color="green"> आप भी इस प्रकार कि कोई भी जानकारी भेज सकते है । हमारा ई-मेल पता है: info@railmagra.com </font></p>
	 
    </div>
  </div>
  
<section id="tables">
 
 <div class="table-responsive">  
  <table class="table table-bordered table-striped table-hover">
    <tbody>
	 <thead>
      <tr>
        <th>क्र. सं.</th>
        <th > ग्राम पंचायत का नाम</th>
		<th > श्रेणी </th>
		 <th> लिंग</th>
      </tr>
    </thead>
    <tbody >
	<?php     
		if ($this->business) {
				$j=1;
				$isSchool = '';
				$type = '';
                foreach($this->business as $key => $value) {
					echo '<tr> <td>';echo $j; echo '</td>';
					echo '<td>';echo $value->hindi_name; echo '</td>';
					echo '<td>';echo $value->category; echo '</td>';
					echo '<td>';echo $value->gender; echo '</td></tr>';
					$j++;
				}
		}
	?>
</tbody>
  </table>
  </div>
<div class="page-header">
    <h6>#TeamRailmagra</h6>
  </div>
</section>

