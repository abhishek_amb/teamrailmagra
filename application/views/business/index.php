<script src="<?php echo URL;?>/public/js/popover.js"></script>


<div class="row">
<?php $this->renderFeedbackMessages(); ?>
    
	<div class="col-md-12">
      <h3>Business Page:</h3>
	
      <p align="justify">Very long ago this area was known as Ganganagar Sub Division which was famous for its production of wheat and cotton in the area. In Mewar Kingdom, this area was very prosperous in agricultural productions. But in recent times, after the construction of Nandsmand Dam on Banas river which was mother of this area as Ganga, the supply of water reduced significantly subsequently resulting in low water level in wells of the area and reduced its agriculture capacity. Because of the above the tube well trend came into picture but the ground level of water is more than one hundred feet which is hazardous to the areas growth perspective. </p>
      <p align="justify">Today, Railmagra is a hub for various businesses ranging from hawkers to industrialists. The biggest employment provider of this area is the mines nurtured by Vedanta Group, primarily konown as Hindustan Zinc Limited. This area is very important source for extraction of various metals like Zinc, Graphite, Cuprum, Glass etc. for HZL. This area also showed the possibility of extraction of Gold and silver.</p>
	  <p align="justify">There are various small businesses which are growing profoundly which are electronics, general stores, garments, jewellery, hardware etc. The taste of various fast foods like Samosa, Kachori, Chaat, Bhel-Puri, Pani-Puri can be taken at any corner of the area with the ice cream parlours and various small restaurants.</p>
      <p align="justify">This village was (roughly estimated) established before 800-900 years back and now it is a town with its worth and will be strong and well developed city in future.</p>
	  
    </div>
  </div>

 <section id="tables">
 
  <div class="page-header">
    <h3>List of Business</h3>
	<button class="btn btn-primary" data-toggle="modal" data-target="#login">
 Add Your Business
</button>
  </div>

  <table class="table table-bordered table-striped table-hover">
    <tbody>
	 <thead>
      <tr>
        <th>Sr</th>
        <th >Shop Name</th>
		 <th>Contact</th>
        <th >Address</th>
		 <th></th>
      </tr>
    </thead>
	
    <tbody >
	<?php     
		if ($this->business) {
				$j=1;
                foreach($this->business as $key => $value) {
				 echo '<tr> <td>';
				echo $j ;
				 
				 echo '</td>';
		        
				
                        if($value->is_premium ==1) {
                        
						echo '<td >'; echo $value->shop_name; echo '<i class="fa fa-info-circle" id="jobskills'; echo $value->ID; echo '" data-trigger="hover" data-container="body" data-toggle="popover"  data-placement="top" data-content="'; echo $value->descptn; echo '" data-html="true"></li></td>
                        <script>
                            $("#jobskills'; echo $value->ID; echo'").popover();
                        </script>';
						
		
						 } else { 
          echo '<td>'; echo $value->shop_name; echo '</td>';
		                }

echo ' <td>'; echo '<i class="fa fa-phone-square"></i>'; echo $value->mobile_no; echo'</td>
		  <td>'; echo $value->address; echo'</td>';
		  
		  
		 if($value->link_url) { 
		  echo '<td> <a class="btn btn-primary" href="'; echo URL; echo $value->link_url; echo '">More..</a></td>';
		   } else { 
		  echo  '<td> </td>';
			 } 
      echo '</tr>';						
                       
				 $j++;
				}
				}
				
		   ?>
	
	</tbody>
  </table>
  
<div class="page-header">
    <h6>List of Business</h6>
  </div>
</section>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">      
  <div class="modal-dialog">
	<div class="modal-content">
  	  <div class="modal-header text-center">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×    </button>
    	<h4 class="modal-title">Enter your Business information</h4>
  	  </div>
	  <div class="modal-body text-center">
        <form class="form-horizontal" role="form" method="post" action="<?php echo URL;?>business/create">
		<div class="form-group">
			<div class="row">
            <label class="col-md-4">Category :</label>
			
            <div class="col-md-6">
             <select name="category_ID" class="form-control input-form" id="category_ID" required>
					<option class="disabled" value="0">Choose Category</option>
					<option value="1">Computer(Sales OR Service)</option>
					<option value="10">Electronics</option>
					<option value="4">Clothing</option>
					<option value="11">Mobile(Sales OR Service)</option>
					<option value="3">Motorcycle(Sales OR Service)</option>
					<option value="7">RealEtate</option>
					<option value="8">Sweet Corner</option>
					<option value="Others">Others</option>
			</select>
            </div>
          </div>
		  <br>
		  <div class="row">
            <label class="col-md-4">Shop Name :</label>
            <div class="col-md-6">
              <input name="shop_name" autocomplete="off" type="text" placeholder="ShopName" title="Enter valid Shop Name" class="form-control" required />
            </div>
          </div>
		   <br>
          
			  <div class="row">
            <label class="col-md-4">Mobile No:</label>
            <div class="col-md-6">
              <input name="mobile_no" autocomplete="off" type="text" pattern="[789][0-9]{9}" title="Enter valid mobile no in 10 digits" placeholder="Mobile No" class="form-control" required />
            </div>
			
          </div>
		  <br>
            <label class="col-md-4">Address:</label>
            <div class="col-md-6">
			  <textarea rows="4" cols="40" autocomplete="off" maxlength="2000" name="address" required class="form-control"></textarea>
            </div>
          </div>
		  
		  <br>
          <div class="form-group">
		   <div class="row">
            <label class="col-md-4">Plan:</label>
			
            <div class="col-md-6">	
					<label class="btn btn-primary">
						<input type="radio"  name="is_premium" value="0" id="is_premium" checked> Free
					</label>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<label class="btn btn-primary">
						<input type="radio"  name="is_premium" value="1" id="is_premium1" > Premium
					</label>
            </div>
			  </div>
			
          </div>
         <br>
		 <div class="form-group" id="competeyes" style="display:none">
			<div class="row">
            <label class="col-md-4">Description:</label>
            <div class="col-sm-6">
             <textarea rows="4" placeholder="Enter your business description(Not Compulsory)." cols="40" maxlength="2000" name="descptn" class="form-control"></textarea>
			 <p style="color:red">we will call you within 24hours for more details.</p>
            </div>
			 </div>
          </div>
         <br>
		 
		 
          
          <input type="submit" class="btn btn-danger" value="Submit">
		  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </form>
      </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script>

document.getElementById("is_premium1").addEventListener("click", function() {
	console.log("here");
       document.getElementById("competeyes").style.display = "block";
    });

	document.getElementById("is_premium").addEventListener("click", function() {
       document.getElementById("competeyes").style.display = "none";
    });
 </script>

