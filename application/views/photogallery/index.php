
<link rel="stylesheet" href="<?php echo URL; ?>public/css/photogallery/blueimp-gallery.min.css">
<link rel="stylesheet" href="<?php echo URL; ?>public/css/photogallery/bootstrap-image-gallery.css">
<style>
#kk{
 width:120px;
 height:130px;
 
}
#kk:hover{
  width:200px;
   height:250px;
 
}
#kk1{
 width:120px;
 height:130px;
 
}
#kk1:hover{
  width:200px;
   height:250px;
 
}#kk2{
 width:120px;
 height:130px;
 
}
#kk2:hover{
  width:200px;
   height:250px;
 
}#kk3{
 width:120px;
 height:130px;
 
}
#kk3:hover{
  width:200px;
   height:250px;
 
}#kk4{
 width:120px;
 height:130px;
 
}
#kk4:hover{
  width:200px;
   height:250px;
 
}#kk5{
 width:120px;
 height:130px;
 
}
#kk5:hover{
  width:200px;
   height:250px;
 
}#kk6{
 width:120px;
 height:130px;
 
}
#kk6:hover{
  width:200px;
   height:250px;
 
}#kk7{
 width:120px;
 height:130px;
 
}
#kk7:hover{
  width:200px;
   height:250px;
 
}#kk8{
 width:120px;
 height:130px;
 
}
#kk8:hover{
  width:200px;
   height:250px;
 
}#kk9{
 width:120px;
 height:130px;
 
}
#kk9:hover{
  width:200px;
   height:250px;
 
}#kk10{
 width:120px;
 height:130px;
 
}
#kk10:hover{
  width:200px;
   height:250px;
 
}#kk11{
 width:120px;
 height:130px;
 
}
#kk11:hover{
  width:200px;
   height:250px;
 
}
</style>
</head>
<body>

<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info pull-left prev">
                      <i class="fa fa-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-info next">
                        Next
                        <i class="fa fa-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="well">
<div id="links">
    <a href="<?php echo URL; ?>/public/img/photogallery/image1_big.jpg" title="Shree Dwarkadhish temple, Sadri(Railmagra)" data-gallery>
        <img  id="kk" class="img-thumbnail" src="<?php echo URL; ?>/public/img/photogallery/image1_big.jpg"  alt="Banana">
    </a>
    <a href="<?php echo URL; ?>/public/img/photogallery/image2_big.jpg" title="Rajiv gandhi circle, Bus Stand Railmagra" data-gallery>
        <img  id="kk1" class="img-thumbnail" src="<?php echo URL; ?>/public/img/photogallery/image2_big.jpg"  alt="Apple">
    </a>
    <a href="<?php echo URL; ?>/public/img/photogallery/image3_big.jpg" title="Office of panchayat samiti Railmagra" data-gallery>
        <img id="kk2" class="img-thumbnail" src="<?php echo URL; ?>/public/img/photogallery/image3_big.jpg"  alt="Orange">
    </a>
	 <a href="<?php echo URL; ?>/public/img/photogallery/image4_big.jpg" title="Shiv Temple, Railmagra" data-gallery>
        <img  id="kk3" class="img-thumbnail" src="<?php echo URL; ?>/public/img/photogallery/image4_big.jpg"  alt="Banana">
    </a>
   
</div>
</div>


<script src="<?php echo URL; ?>public/js/photogallery/jquery.blueimp-gallery.min.js"></script>
<script src="<?php echo URL; ?>public/js/photogallery/bootstrap-image-gallery.js"></script>

