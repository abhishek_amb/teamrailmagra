
 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
             <img src="<?php echo URL; ?>/public/img/religious_more/matrikundia - mewar ka haridwar - religious places-by railmagra team.jpg" width="800" height="338" alt="Second slide">
          </div>
          <div class="item">
            <img src="<?php echo URL; ?>/public/img/religious_more/matrikundia - mewar ka haridwar - religious places-by railmagra team.jpg" width="800" height="338" alt="Second slide">
          </div>
          <div class="item">
            <img src="<?php echo URL; ?>/public/img/religious_more/matrikundia - mewar ka haridwar - religious places-by railmagra team.jpg" width="800" height="338" alt="Third slide">
          </div>
        </div>
       <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          &lsaquo;
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
         &rsaquo;
        </a>
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          &lsaquo;
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
         &rsaquo;
        </a>
      </div>
<div class="row">


    <div class="well">
      
        <h3>Matrikundia (Mewar Ka Haridwar)</b></h3>
		 <p align="justify"><font size="3">This is the most religious place which is situated in Railmagra Subdivision surrounded by the borders of Bhilwara, Chittorgarh and Rajasmand, also known as “Mewar ka Haridwar”. There are many small temples including the main temple of ‘Mangleshwar Mahadev’ situated at Parshuram Kund.   </font></p>
		  <p align="justify"><font size="3">Matrikundia is famous for its story of Parshuram coming here after murdering his mother for getting rid of his guilty and bathed here for being holy for that sin committed by him. Many of the people from Mewar Region dispose their predecessor’s bones at here. </font></p>
		  <p align="justify"><font size="3">There is also Matrikundia Dam which is built on 52 feet height on plain land which is India’s First and World’s Third Mega Dam. This is the most attractive place for tourists coming here. A lots of people visits this dam every year. </font></p>
      		 
	
   </div>
   </div>
   <div class="row" style="float:right;">
    
   <p align="justify"><font size="3"><b><u>Thanks to : Mr. Sunil kumar Tak</u> -> &nbsp; <a href="<?php echo URL; ?>religious/matrikundia_hindi"><u>Hindi View</u></a></b></font></div>
  