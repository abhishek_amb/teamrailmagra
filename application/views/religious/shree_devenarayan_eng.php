	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
             <img src="<?php echo URL; ?>/public/img/religious_more/devnarayan mandir - pachmata - railmagra - 313329-2.jpg" width="800" height="338" alt="Second slide">
          </div>
          <div class="item">
            <img src="<?php echo URL; ?>/public/img/religious_more/devnarayan mandir - pachmata - railmagra - 313329-2.jpg" width="800" height="338" alt="Second slide">
          </div>
          <div class="item">
            <img src="<?php echo URL; ?>/public/img/religious_more/devnarayan mandir - pachmata - railmagra - 313329-2.jpg" width="800" height="338" alt="Third slide">
          </div>
        </div>
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          &lsaquo;
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
         &rsaquo;
        </a>
      </div>
    
<div class="row">
 
      
        	  
			
		
		<div class="col-md-12">
		<h3>Shree Devnarayan Temple , Pachmata(Railmagra)</b></h3>
           <p align="justify"><font size="3">This temple is situated at Pachmata village which 9 kms. Away from Railmara and 40 Kms. from Rajsamand. A lot of pilgrims have deep faith in God Devnaryan. There are also other temples adjoined with Devnarayan temple which are Gajanand Ji, Mata Sadhu, Khakhaldevji, Kalaji Maharaj, Kala Gora Bherunath, Mehnduji, Bhunaji, Bhangde Kanji, Madanji etc. The god Devnarayn is saviour for all but especially he is known as the saviour from poisonous animals and dog bite. When someone comes to him with belief he takes the responsibility to cure.  There is huge rush for all the 365 days in the year. There is facility of shelters for the pilgrims.    </font></p>
		  <p align="justify"><font size="3">A ticket is also issued by the Govt. Of India in 1992 being influenced by the fame of Devnarayan. There is no strong evidence about the history of the temple but based on the stories of ancestors and elders it is believed that nearly 200 years ago some thieves theft the Cow from Khemana Village (Near to Rajaji ka Karera). The God Devnarayan came to save the cow and he was returning with the cow. In the way he felt thirsty at 4.15 am and appealed for water from the villagers. At that time, a lady named Omibai appeared with a jug of Water and a bowl of milk to serve him. Being pleased with the courtship of lady he pointed her place and said that he will be there and disappeared. </font></p>
		  <p align="justify"><font size="3">Omibai went to her house and told the whole story to her brothers Thanaji and Chandaji.  Thanaji and Chandaji started digging that place in the hunt and hope of treasure but found “Shankh, Worship Plate and Sankal” and disappeared immediately. Looking at this instance the brothers came to know about the truth of the cow man and established a temple of Devnarayan at here. The predecessors of the brothers are the priests of the temple. There is huge worship is conducted twice in the year and on that day not single person from the village sales milk. The whole milk is used for making “Kheer” as Prasad to the God Devnarayan. <br /> </font></p>
       
        </div>
	    </div>
		
		<div class="row" style="float:right;">
    
   <p align="justify"><font size="3"><b><u>Thanks to : Mr. Sunil kumar Tak</u> -> &nbsp; <a href="<?php echo URL; ?>religious/shree_devenarayan_hindi"><u>Hindi View</u></a></b></font></div>
  