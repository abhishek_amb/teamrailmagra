
	
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
             <img src="<?php echo URL; ?>/public/img/religious_more/jaldevi mandir Sansera - railmagra - Copy.jpg" width="400" height="300" alt="Second slide">
          </div>
          <div class="item">
            <img src="<?php echo URL; ?>/public/img/religious_more/jaldevi mandir Sansera - railmagra - Copy.jpg" width="400" height="300" alt="Second slide">
          </div>
          <div class="item">
            <img src="<?php echo URL; ?>/public/img/religious_more/jaldevi mandir Sansera - railmagra - Copy.jpg" width="400" height="300" alt="Third slide">
          </div>
        </div>
       <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          &lsaquo;
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
         &rsaquo;
        </a>
      </div>
    
<div class="row">


    
    <div class="col-md-12">
      
         <h3>Historical Place – Jaldevi Mata Temple, Sansera</b></h3>
		  
       
          <p align="justify"><font size="3">This place is situated in Railmagra Sub Division which is 25 Kms. from “Nau Chowki” Monument at district headquarter and 22 Kms. from the famous holy place of Shrinathji. This just 15 kms. away from Railmagra. There is historical temple of Jaldevi Mata in middle of the pond which is nearly 1000 years old. This place is also related to Maharana Pratap. There are various small temples on the banks of the pond such as ‘Bankya Rani Temple’, ‘Bheruji Temple’, ‘ Mamadev ki Chatri’ etc.   </font></p>
		  <p align="justify"><font size="3">The wall along the pond is 9 kms. long and is beautifully sculptured in historic style and seems like a human. The speciality of this pond is the flow of water is in same direction while both coming towards the pond and going out of the pond. There is a bridge type of wall which is used for going to the temple in the middle of the pond.</font></p>
		  <p align="justify"><font size="3">In the month of “Chaitra Navratri” a fair of 4 days is organised from last 29 years. The famous thing of this temple is that the lamp of the temple is lighted from water instead of oil. This place is between Chittorgarh and Haldigathi. When the Akbar was returning after winning Chittorgarh he stopped at here and his army stopped at nearby hills.<br /></font></p>
       
     
	  
   </div>
  </div>
  <div class="row" style="float:right;">
    
   <p align="justify"><font size="3"><b><u>Thanks to : Mr. Sunil kumar Tak</u> -> &nbsp; <a href="<?php echo URL; ?>religious/jaldevi_hindi"><u>Hindi View</u></a></b></font></div>