<?php

/**
 * BusinessModel
 *
 * Handles the user's login / logout / registration stuff
 */
use Gregwar\Captcha\CaptchaBuilder;

class EducationModel
{
    /**
     * Constructor, expects a Database connection
     * @param Database $db The Database object
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }
	
	 public function getAllSchool()
    {
        $sql = "Select * from school_info where is_activate = 1 order by is_premium desc";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        // fetchAll() is the PDO method that gets all result rows
        return $query->fetchAll();
    }
	
	 public function create($is_gov,$school_name,$category_edu,$mobile_no,$address,$is_premium,$descptn)
    {
        // clean the input to prevent for example javascript within the notes.
        $school_name = strip_tags($school_name);
		$address = strip_tags($address);
		$desptn = strip_tags($descptn);
        $sql = "INSERT INTO school_info (is_gov, school_name, category_edu, mobile_no, address, is_premium, descptn)
VALUES (:is_gov, :school_name, :category_edu,:mobile_no, :address,:is_premium, :descptn)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':is_gov' => $is_gov, ':school_name' => $school_name, ':category_edu' => $category_edu,':mobile_no' => $mobile_no, ':address' => $address,':is_premium' => $is_premium, ':descptn' => $desptn));
        $count =  $query->rowCount();
        if ($count == 1) {
			$_SESSION["feedback_positive"][] = FEEDBACK_SCHOOL_CREATION_SUCCESSFUL;
        } else {
            $_SESSION["feedback_negative"][] = FEEDBACK_NOTE_CREATION_FAILED;
        }
        // default return
        return false;
    }
}
