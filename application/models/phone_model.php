<?php

/**
 * BusinessModel
 *
 * Handles the user's login / logout / registration stuff
 */
use Gregwar\Captcha\CaptchaBuilder;

class PhoneModel
{
    /**
     * Constructor, expects a Database connection
     * @param Database $db The Database object
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }
	
	 public function getAllNumbers()
    {
        $sql = "Select * from phone where is_activate = 1 order by is_premium desc, category_ID";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        // fetchAll() is the PDO method that gets all result rows
        return $query->fetchAll();
    }
	
	 public function create($category_ID,$name,$mobile_no,$phone_no,$others)
    {
        $others = strip_tags($others);
        $sql = "INSERT INTO phone ( name, mobile_no, phone_no, category_ID, others) VALUES ( :name, :mobile_no, :phone_no, :category_ID, :others)";
        $query = $this->db->prepare($sql);
        $query->execute(array( ':name' => $name,':mobile_no' => $mobile_no, ':phone_no' => $phone_no, ':category_ID' => $category_ID, ':others' => $others));
		$count =  $query->rowCount();
        if ($count == 1) {
			$_SESSION["feedback_positive"][] = FEEDBACK_PHONE_CREATION_SUCCESSFUL;
        } else {
            $_SESSION["feedback_negative"][] = FEEDBACK_PHONE_CREATION_FAILED;
        }
        // default return
        return false;
    }
}
