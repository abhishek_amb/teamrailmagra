<?php

/**
 * BusinessModel
 *
 * Handles the user's login / logout / registration stuff
 */
use Gregwar\Captcha\CaptchaBuilder;

class QuestionModel
{
    /**
     * Constructor, expects a Database connection
     * @param Database $db The Database object
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }
	
	 public function getAllQuestion()
    {
		
        $sql = " SELECT * FROM answer where is_active=1";
        $query = $this->db->prepare($sql);
        $query->execute();
		
		$sql2 = " SELECT * FROM question where is_active=1";
        $query2 = $this->db->prepare($sql2);
        $query2->execute();
		
		$a=array("answer"=> $query->fetchAll() ,"question"=> $query2->fetchAll());
        return $a;
    }
	
	 public function create($name,$email,$question)
    {
        $name = strip_tags($name);
		$email= strip_tags($email);
		$question = strip_tags($question);
		$date = date('Y-m-d H:i:s');
		
		$sql = "INSERT INTO question (question, posted_by, posted_email,posted_date)
VALUES (:question, :posted_by,:posted_email,:posted_date)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':question' => $question, ':posted_by' => $name,':posted_email' => $email, ':posted_date' => $date));
        $count =  $query->rowCount();
		
        if ($count == 1) {
			$_SESSION["feedback_positive"][] = FEEDBACK_NOTE_QUESTION_SUCCESSFUL;
        } else {
            $_SESSION["feedback_negative"][] = FEEDBACK_NOTE_QUESTION_FAILED;
        }
        // default return
        return false;
    }
	
	 public function answer($name,$email,$answer,$question_id)
    {
        $name = strip_tags($name);
		$email= strip_tags($email);
		$answer = strip_tags($answer);
		$date = date('Y-m-d H:i:s');
		
		$sql = "INSERT INTO answer (answer, posted_by, posted_email,posted_date, question_id)
VALUES (:answer, :posted_by,:posted_email,:posted_date,:question_id)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':answer' => $answer, ':posted_by' => $name,':posted_email' => $email, ':posted_date' => $date, ':question_id' => $question_id));
        $count =  $query->rowCount();
		
        if ($count == 1) {
			$_SESSION["feedback_positive"][] = FEEDBACK_NOTE_ANSWER_SUCCESSFUL;
        } else {
            $_SESSION["feedback_negative"][] = FEEDBACK_NOTE_ANSWER_FAILED;
        }
        // default return
        return false;
    }
}
