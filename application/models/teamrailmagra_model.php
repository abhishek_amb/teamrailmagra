<?php

/**
 * BusinessModel
 *
 * Handles the user's login / logout / registration stuff
 */
use Gregwar\Captcha\CaptchaBuilder;

class TeamrailmagraModel
{
    /**
     * Constructor, expects a Database connection
     * @param Database $db The Database object
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }
	
	 public function getAllMembers()
    {
        $sql = "Select * from team_railmagra where is_active = 1 order by priority";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	 public function create($name,$occupation,$mobile_no,$photo_url)
    {
        $name = strip_tags($name);
		$occupation = strip_tags($occupation);
		
        $sql = "INSERT INTO team_railmagra ( name, occupation, mobile_no, photo_url)
VALUES (:name, :occupation,:mobile_no, :photo_url)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':name' => $name, ':occupation' => $occupation,':mobile_no' => $mobile_no, ':photo_url' => $photo_url));
        $count =  $query->rowCount();
        if ($count == 1) {
			$_SESSION["feedback_positive"][] = FEEDBACK_MEMBER_CREATION_SUCCESSFUL;
        } else {
            $_SESSION["feedback_negative"][] = FEEDBACK_MEMBER_CREATION_FAILED;
        }
        // default return
        return false;
    }
}
