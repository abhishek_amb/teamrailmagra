<?php

/**
 * BusinessModel
 *
 * Handles the user's login / logout / registration stuff
 */
use Gregwar\Captcha\CaptchaBuilder;

class BusinessModel
{
    /**
     * Constructor, expects a Database connection
     * @param Database $db The Database object
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }
	
	 public function getAllList()
    {
        $sql = "Select * from grampanchayatlist order by english_name desc";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	 public function getAllBusiness()
    {
        $sql = "Select * from business_owner_info where is_activate = 1 order by is_premium desc";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        // fetchAll() is the PDO method that gets all result rows
        return $query->fetchAll();
    }
	
	 public function create($category_ID,$shop_name,$mobile_no,$address,$is_premium,$descptn)
    {
        // clean the input to prevent for example javascript within the notes.
        $shop_name = strip_tags($shop_name);
		$address = strip_tags($address);
		$desptn = strip_tags($descptn);
		$mobile_no = "+91-"+$mobile_no;
        $sql = "INSERT INTO business_owner_info (category_ID, shop_name, mobile_no, address, is_premium, descptn)
VALUES (:category_ID, :shop_name,:mobile_no, :address,:is_premium, :descptn)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':category_ID' => $category_ID, ':shop_name' => $shop_name,':mobile_no' => $mobile_no, ':address' => $address,':is_premium' => $is_premium, ':descptn' => $desptn));
        $count =  $query->rowCount();
        if ($count == 1) {
			$_SESSION["feedback_positive"][] = FEEDBACK_NOTE_CREATION_SUCCESSFUL;
        } else {
            $_SESSION["feedback_negative"][] = FEEDBACK_NOTE_CREATION_FAILED;
        }
        // default return
        return false;
    }
}

