-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2014 at 06:55 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `railmagra_city`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(500) NOT NULL,
  `posted_by` varchar(500) NOT NULL,
  `like_count` int(11) NOT NULL DEFAULT '0',
  `unlike_count` int(11) NOT NULL DEFAULT '0',
  `posted_email` varchar(500) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `posted_date` date NOT NULL,
  `question_id` int(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `answer`, `posted_by`, `like_count`, `unlike_count`, `posted_email`, `is_active`, `posted_date`, `question_id`) VALUES
(1, 'My name is surendra', 'Virat Kohli', 1, 0, 'surendrakhatikk24@gmail.com', 1, '2014-11-25', 1),
(2, 'My name is surendra', 'Virat Kohli', 1, 0, 'surendrakhatikk24@gmail.com', 1, '2014-11-25', 1),
(3, 'My name is surendra', 'Virat Kohli', 1, 0, 'surendrakhatikk24@gmail.com', 1, '2014-11-25', 1),
(4, 'My name is surendra', 'Virat Kohli', 1, 0, 'surendrakhatikk24@gmail.com', 1, '2014-11-25', 1),
(5, 'QFGDFGFD', 'sURENDRA', 0, 0, 'KUMAR', 1, '2014-11-27', 2),
(6, 'QDGSDG', 'aBHISHEK', 0, 0, 'AMB', 1, '2014-11-27', 2);

-- --------------------------------------------------------

--
-- Table structure for table `business_category`
--

CREATE TABLE IF NOT EXISTS `business_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `business_category`
--

INSERT INTO `business_category` (`ID`, `name`) VALUES
(1, 'Computer'),
(3, 'Cars'),
(4, 'Clothing'),
(6, 'Motorcycle'),
(7, 'RealEstate'),
(8, 'Sweet Corner'),
(9, 'others');

-- --------------------------------------------------------

--
-- Table structure for table `business_owner_info`
--

CREATE TABLE IF NOT EXISTS `business_owner_info` (
  `ID` int(100) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(100) NOT NULL,
  `mobile_no` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `is_premium` int(20) NOT NULL DEFAULT '0',
  `link_url` varchar(200) NOT NULL,
  `submitted_date` datetime NOT NULL,
  `category_ID` int(200) NOT NULL,
  `descptn` varchar(500) DEFAULT NULL,
  `is_activate` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `business_owner_info`
--

INSERT INTO `business_owner_info` (`ID`, `shop_name`, `mobile_no`, `address`, `is_premium`, `link_url`, `submitted_date`, `category_ID`, `descptn`, `is_activate`) VALUES
(50, 'Neelkamal Infotech', '+91-9460113389', 'Near vodafone tower, Fatehnagar Road, Railmagra', 1, 'teamrailmagra/NeelKamalInfotechRailmagra', '0000-00-00 00:00:00', 1, 'We hava all types of computer sales, spares and Service. Owner of this firm is Mr VINIT UPADHYAY', 1),
(51, 'Shree Goverdhan Web Infotech', '+91-9660245223', 'Hospital Road, Railmagra', 0, '', '0000-00-00 00:00:00', 10, 'Sorry Not Premium', 1),
(52, 'Choice A-1 Fabrication', '+91-9828744383', 'Near Bartiya Shishu Niketan,Kanroli Road, Railmagra', 1, 'teamrailmagra/ChoiceAone', '0000-00-00 00:00:00', 0, 'We are known for latest design in railmagra. Owner of this firm Mr Iqbal Hussain', 1),
(53, 'Raza Emporium', '+91-9414685688', 'Bus stand, Railmagra', 1, 'teamrailmagra/RazaEmporiumRailmagra', '0000-00-00 00:00:00', 4, 'We have all types of suits,saris, under garments and kids wear. Owner of this firm is Mr Fatehmohd Rangrej.', 1),
(54, 'TUKLIYA & CO, CHARTERED ACCOUNTANT', '+91-9414927819', 'Kankroli Road, Railmagra', 1, '', '0000-00-00 00:00:00', 0, 'Owner of this firm Mr CA Ronak Tukliya.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `business_sub_category`
--

CREATE TABLE IF NOT EXISTS `business_sub_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `business_category_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `business_sub_category`
--

INSERT INTO `business_sub_category` (`ID`, `name`, `business_category_id`) VALUES
(1, 'Men''s Clothing', 4),
(2, 'Women''s Clothing', 4),
(3, 'Kid''s Clothing ', 4),
(4, 'Men''s, Women''s and Kid''s', 4),
(5, 'Sofware Service', 1),
(6, 'Hardware Service', 1),
(7, 'Sell and Service', 1),
(8, 'Service', 3),
(9, 'Sell', 3),
(10, 'Sell and Service', 3),
(11, 'Sell', 6),
(12, 'Service', 6),
(13, 'Sell and Service', 6);

-- --------------------------------------------------------

--
-- Table structure for table `guestbook`
--

CREATE TABLE IF NOT EXISTS `guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `guestbook`
--

INSERT INTO `guestbook` (`id`, `date_time`, `name`, `email`, `comment`) VALUES
(1, '2014-11-23 12:40:03', 'sdgsdgsd', 'qdfhhf', 'fzvzv'),
(2, '2014-11-23 12:40:10', 'dsgd', 'dsgsdg', 'sdgs'),
(3, '2014-11-23 12:49:15', 'dfgdfqd', 'segs', 'sdgs'),
(4, '2014-11-23 12:49:24', 'sdfsd', 'sdgs', 'sdgsdgs'),
(5, '2014-11-23 12:49:34', 'xgffxd`', 'sadgsd', 'sdgsd'),
(6, '2014-11-23 12:49:44', 'fhdfh', 'sdgsdg', 'sdgsd');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `qualification` varchar(100) DEFAULT NULL,
  `ref_link` varchar(50) DEFAULT NULL,
  `website_link` varchar(50) DEFAULT NULL,
  `is_govt` int(10) NOT NULL DEFAULT '0',
  `vacancy_count` int(30) DEFAULT NULL,
  `visitor_count` int(50) NOT NULL DEFAULT '0',
  `last_date` date DEFAULT NULL,
  `is_activate` int(10) NOT NULL DEFAULT '1',
  `key_skill` varchar(500) DEFAULT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `description`, `qualification`, `ref_link`, `website_link`, `is_govt`, `vacancy_count`, `visitor_count`, `last_date`, `is_activate`, `key_skill`, `company_name`) VALUES
(1, 'java developer', 'joijnoi iojnmoinio oijnoinjmio ijmnoi', 'nojnojnjok onojknojknl onojinol onol', 'www.jiojio.com', 'huihuihiu.npm.kj', 0, 5, 0, '2014-11-12', 1, 'jjk,i8huj8ij,iji', 'Infosys'),
(2, 'gram panchayat', 'w4etrwetwetre', 'reteryty', 'www.got', 'www.govt', 1, 800, 0, '2014-11-20', 1, 'ertretyertert tertertret', 'Infosys');

-- --------------------------------------------------------

--
-- Table structure for table `phone`
--

CREATE TABLE IF NOT EXISTS `phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `mobile_no` bigint(20) DEFAULT NULL,
  `phone_no` varchar(100) NOT NULL,
  `category_ID` int(11) DEFAULT NULL,
  `is_activate` int(11) NOT NULL DEFAULT '1',
  `others` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `phone`
--

INSERT INTO `phone` (`id`, `name`, `mobile_no`, `phone_no`, `category_ID`, `is_activate`, `others`) VALUES
(1, 'Surendra kumar', 9660245223, '02952-267673', 1, 1, NULL),
(2, 'Surendra kumar', 9660245223, '02952-267673', 1, 1, NULL),
(3, 'cfdf', 9999999999, 'sdgsd', 1, 1, NULL),
(4, 'sdgdsq', 8989898989, 'dfgsd', 3, 1, 'fbd');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(500) NOT NULL,
  `posted_by` varchar(100) NOT NULL,
  `like_count` int(200) DEFAULT '0',
  `unlike_count` int(200) NOT NULL DEFAULT '0',
  `posted_email` varchar(200) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `posted_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question`, `posted_by`, `like_count`, `unlike_count`, `posted_email`, `is_active`, `posted_date`) VALUES
(1, 'Hello Surendra', 'Kumar Surendra', 1, 1, 'surendrakhatikk24@gmail.com', 1, '2014-11-25'),
(2, 'Hello Surendra', 'Kumar Surendra', 1, 1, 'surendrakhatikk24@gmail.com', 1, '2014-11-25'),
(3, 'ambkk24sdgg', 'Abhishek', 0, 0, NULL, 1, '0000-00-00'),
(4, 'sdgdsgsdg', 'sdgdsgd', 0, 0, 'sdfsgfsd', 1, '0000-00-00'),
(5, 'dsgdsgsds', 'sgdsg', 0, 0, 'asfsdfsf', 1, '2014-11-27');

-- --------------------------------------------------------

--
-- Table structure for table `religious_places`
--

CREATE TABLE IF NOT EXISTS `religious_places` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) NOT NULL,
  `image` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `is_activate` int(11) NOT NULL DEFAULT '0',
  `more` varchar(150) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `religious_places`
--

INSERT INTO `religious_places` (`ID`, `name`, `image`, `link`, `is_activate`, `more`) VALUES
(1, 'Matrikundia', 'img/religious/matrikundia-railmagra.jpg', 'http://railmagra.in/railmagra/matrikundia', 1, 'Read more..'),
(2, 'Chamunda devi temple, Railmagra', 'img/religious/Chamunda devi temple, Railmagra- relious places - railmagra.jpg', 'http://railmagra.in/railmagra/chamunda_mata', 1, 'Read more..'),
(3, 'Devnarayan Temple (Pachmata)', 'img/religious/devnarayan mandir - pachmata - railmagra - 313329.jpg', 'http://railmagra.in/railmagra/devnarayan_temple', 1, 'Read more..'),
(4, 'Jaldevi Mata Temple, Sansera', 'img/religious/jaldevi mandir Sansera - railmagra.jpg', 'http://railmagra.in/railmagra/jaldevi_mata', 1, 'Read more..');

-- --------------------------------------------------------

--
-- Table structure for table `school_category`
--

CREATE TABLE IF NOT EXISTS `school_category` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `school_category`
--

INSERT INTO `school_category` (`ID`, `name`) VALUES
(1, 'Government'),
(2, 'Private');

-- --------------------------------------------------------

--
-- Table structure for table `school_info`
--

CREATE TABLE IF NOT EXISTS `school_info` (
  `ID` int(100) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(200) NOT NULL,
  `category_edu` int(11) DEFAULT NULL,
  `mobile_no` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `is_premium` int(20) NOT NULL DEFAULT '0',
  `link_url` varchar(200) NOT NULL,
  `submitted_date` datetime NOT NULL,
  `descptn` varchar(500) NOT NULL,
  `is_activate` int(11) NOT NULL DEFAULT '0',
  `is_gov` int(40) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `team_railmagra`
--

CREATE TABLE IF NOT EXISTS `team_railmagra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `role` varchar(200) NOT NULL DEFAULT 'member',
  `occupation` varchar(200) DEFAULT NULL,
  `mobile_no` bigint(20) DEFAULT NULL,
  `created_date` date NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) DEFAULT NULL,
  `user_type` int(11) NOT NULL DEFAULT '3',
  `user_name` varchar(300) DEFAULT NULL,
  `password` varchar(300) DEFAULT NULL,
  `photo_url` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `team_railmagra`
--

INSERT INTO `team_railmagra` (`id`, `name`, `role`, `occupation`, `mobile_no`, `created_date`, `is_active`, `priority`, `user_type`, `user_name`, `password`, `photo_url`) VALUES
(1, 'Surendra kumar', 'Project Initiator, Developer', 'Software engineer', 9660245223, '2014-11-29', 1, 1, 1, 'surkk24', '12345678', 'suri.jpg'),
(2, 'Abhishek kumar amb', 'Designer and developer', 'Software engineer', 9660245223, '2014-11-29', 1, 1, 1, 'amb24', '1234', 'abhi.jpg'),
(3, 'fghfg', 'member', 'dfhdf', 9660245223, '0000-00-00', 0, NULL, 3, NULL, NULL, NULL),
(4, 'fhjf', 'member', 'dfgdf', 8989898989, '0000-00-00', 0, NULL, 3, NULL, NULL, NULL),
(5, 'fhjf', 'member', 'dfgdf', 8989898989, '0000-00-00', 0, NULL, 3, NULL, NULL, NULL),
(6, 'fhfd', 'member', 'gd', 7878787878, '0000-00-00', 0, NULL, 3, NULL, NULL, 'Untitled.png'),
(7, 'dfhdf', 'member', 'df', 7878787878, '0000-00-00', 0, NULL, 3, NULL, NULL, 'railmagra_.jpg'),
(8, 'cfh', 'member', 'qfhf', 8989898989, '0000-00-00', 0, NULL, 3, NULL, NULL, 'Untitled.png');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
